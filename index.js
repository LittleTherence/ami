const { Client, Collection, ActivityType, Events, ContextMenuCommandBuilder, ApplicationCommandType } = require('discord.js')
require('dotenv').config()

const fs = require("fs");
const client = new Client({
    intents: 3276799
});

client.commands = new Collection()
const commandFiles = fs.readdirSync('./commands').filter(file =>  file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);

    if(!command.name || typeof command.name !== 'string') throw new TypeError(`La commande ${file.splice(0, file.length - 3)} n'a pas de nom !`)

    client.commands.set(command.name, command);
}

client.login(process.env.TOKEN)

client.once('ready', () => {
    console.log(`${client.user.tag} is ready`)

    client.user.setPresence({
        activities: [{ name: `le pétard de Zoé`, type: ActivityType.Watching }],
        status: 'dnd',
    });
})

client.on('messageCreate', message => {
    if(!message.content.startsWith(process.env.PREFIX) || message.author.bot) return;

    const args = message.content.slice(process.env.PREFIX.length).split(/ +/);
    const command = args.shift().toLowerCase();

    if (!client.commands.has(command)) {
        return message.reply(`Commande non reconnue ! \nUtilisez \`${process.env.PREFIX}help\` pour voir la liste des commandes`)
    }

    try {
        client.commands.get(command).execute(client, message, args)
    } catch (error) {
        console.error(error);
        message.reply('Il y a eu une erreur !')
    }
})

client.on(Events.InteractionCreate, async interaction => {
    const interactionFile = require(`./interactions/${interaction.customId.toLowerCase()}`)
    interactionFile.execute(interaction)
});

const data = new ContextMenuCommandBuilder()
    .setName('User Information TT')
    .setType(ApplicationCommandType.User);
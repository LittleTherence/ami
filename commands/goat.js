const Discord = require('discord.js');

module.exports = {
    name: 'goat',
    description: 'Qui est le GOAT ?',
    async execute(client, message, args) {

        const Guilds = client.guilds.cache.map(guild => guild.id);

        const guild = client.guilds.cache.get(Guilds[0]);
        let res = await guild.members.fetch();

        // res.forEach((member) => {
        //     console.log(member.user);
        // });

        const memberList = res.map(x => {
            return {
                label: x.user.username,
                description: x.user.username,
                value: x.user.id,
                bot: x.user.bot
            }
        })

        const memberListFilterd = memberList.filter(x => x.bot == false);

        const row = new Discord.ActionRowBuilder()
            .addComponents(
                new Discord.StringSelectMenuBuilder()
                    .setCustomId('selectgoat')
                    .setPlaceholder('Personne selectionnée')
                    // .setMinValues(2)
                    // .setMaxValues(3)
                    .addOptions(memberListFilterd),
            );

        await message.reply({ content: 'Qui est le goat de ce channel ?', components: [row] });
    }
}
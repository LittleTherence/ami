const Discord = require('discord.js');
const fs = require('fs');
require('dotenv').config()

module.exports = {
    name: 'help',
    description: 'Liste des commandes',
    async execute(client, message, args) {
        const commandFiles = fs.readdirSync('./commands').filter(file =>  file.endsWith('.js'));

        const commandList = commandFiles.map(x => {
            const command = require(`../commands/${x.slice(0, -3)}`);

            return  {
                name: `${process.env.PREFIX}${command.name}`,
                value: command.description
            }
        })

        const exampleEmbed = new Discord.EmbedBuilder()
            .setColor(0x0099FF)
            .setTitle('Help')
            // .setURL('https://discord.js.org/')
            .setAuthor({ name: 'AMI', iconURL: process.env.PP, url: 'https://renssethe.fr/' })
            .setDescription('Liste des commandes')
            .setThumbnail(process.env.PP)
            .addFields(commandList)
            // .addFields({ name: 'Inline field title', value: 'Some value here', inline: true })
            // .setImage(process.env.PP)
            .setTimestamp()
            .setFooter({ text: client.user.tag, iconURL: process.env.PP });

        message.channel.send({ embeds: [exampleEmbed] });
    }
}
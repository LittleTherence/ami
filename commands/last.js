const Discord = require('discord.js');
const fetch = require("node-fetch");

module.exports = {
    name: 'last',
    description: 'Last last.fm played song by LittleTherence',
    async execute(client, message, args) {


        const apiKey = '90391df1507d6b372845cbcdce690d94';
        const username = 'LittleTherence';
        const url = `https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=${username}&api_key=${apiKey}&format=json`;

        fetch(url)
            .then(response => response.json())
            .then(async data => {
                const recentTracks = data.recenttracks.track;
                const lastTrack = recentTracks[0]

                const exampleEmbed = new Discord.EmbedBuilder()
                    .setColor("#d0232b")
                    .setTitle('Last.fm')
                    .setAuthor({ name: username, iconURL: "https://play-lh.googleusercontent.com/VFmAfWqcuV3aReZG8MMQdHRSdKWx85IW22f4RQ5xhR5U-o1_u03P7TVwsnTYa26Q1No", url: `https://last.fm/user/${username}` })
                    .setDescription('Dernier son joué:')
                    .setThumbnail(lastTrack.image[1]['#text'])
                    .addFields({
                        name: lastTrack.artist['#text'],
                        value: lastTrack.name
                    })
                    .setTimestamp()
                    .setFooter({ text: client.user.tag, iconURL: process.env.PP });

                    message.channel.send({ embeds: [exampleEmbed] });
            })
            .catch(error => {
                console.error(error)
            });
    }
}
